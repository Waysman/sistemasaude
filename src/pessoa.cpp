#include "../inc/pessoa.hpp"

#include <iostream>
#include <string>

Pessoa::Pessoa(){

}
Pessoa::Pessoa(string nome, int idade){
	this->idade=idade;
	this->nome=nome;
}
string getNome(){
	return nome;
}
void setNome(string nome){
	this->nome=nome;
}
int getIdade(){
	return idade;
}
void setIdade(int idade){
	this->idade=idade;
}

#ifndef PESSOA_HPP
#define PESSOA_HPP

#include <string>

class Pessoa{
	private:
		string nome;
		int idade;
	public:
		Pessoa();
		Pessoa(string nome, int idade);
		string getNome();
		void setNome();
		int getIdade();
		void setIdade();
};



#endif
